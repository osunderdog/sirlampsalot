

import unittest

## GPIO  if not available, then should provide some stub
import RPi.GPIO as GPIO

class TestGPIO(unittest.TestCase):
    
    def test_setmode(self):
        GPIO.setmode(GPIO.BCM)
        self.assertTrue("setmode")

    def test_setup(self):
        GPIO.setup(self.PCB_GREEN_BUTTON, GPIO.IN)
        self.assertTrue("setmode")

    def test_add_event(self):
        GPIO.add_event_detect(self.PCB_GREEN_BUTTON,
                              GPIO.RISING, 
                              func,
                              bouncetime=200)
        self.assertTrue("add event")

    def test_inputCheck(self):
        GPIO.input(self.PCB_GREEN_BUTTON)
        self.assertTrue("input check")

suite = unittest.TestLoader().loadTestsFromTestCase(TestGPIO)
unittest.TextTestRunner(verbosity=2).run(suite)
