import unittest
import picamera
import time
import PICameraCapture

class UniqueDict(dict):
    def __setitem__(self, key, value):
        if key not in self:
            dict.__setitem__(self, key, value)
        else:
            raise KeyError("Key already exists")

class TestPICameraCapture(unittest.TestCase):
        def test_01CreateInstance(self):
            camera = PICameraCapture.PICameraCapture()
            self.assertIsInstance(camera, PICameraCapture.PICameraCapture, "is valid PICameraCapture Instance.")

        def test_02CreateUniqueFilename(self):
            camera = PICameraCapture.PICameraCapture()
            fn = camera.uniqueImageFilename()
            self.assertIsNot(fn, '', 'There is something there')
            
        def test_03CreateUniqueFilenameList(self):
            camera = PICameraCapture.PICameraCapture()
            ##calling uniqueImageFilename several times very quickly should still result in unique filenames.
            unidict = UniqueDict()
            rangeCheck = 100

            try:
                for i in range(rangeCheck):
                    key = camera.uniqueImageFilename()
                    unidict[key] = 1 
            except KeyError:
                print(key)
            ##should throw an exception if it hits a duplicate
            self.assertEqual(len(unidict.keys()), rangeCheck, "not all keys are unique")

        def test_04Capture(self):
            camera = PICameraCapture.PICameraCapture()
            fn = camera.capture('bw',2)
            self.assertIsNot(fn, '', 'there is something there')

        def test_05Capture(self):
            camera = PICameraCapture.PICameraCapture()
            camera.cameraConfigDict['bw'] = PICameraCapture.bwCamera
            fn = camera.capture('bw',2)
            self.assertIsNot(fn, '', 'there is something there')

        def test_06Capture(self):
            camera = PICameraCapture.PICameraCapture()
            camera.cameraConfigDict['bw'] = PICameraCapture.bwCamera
            fn = camera.capture('bw')
            self.assertIsNot(fn, '', 'there is something there')  
                      
suite = unittest.TestLoader().loadTestsFromTestCase(TestPICameraCapture)
unittest.TextTestRunner(verbosity=2).run(suite) 