import unittest
import ButtonBar

class Object:
    value = False
    
    def __init__(self, v):
        self.value = v
    
    def setValue(self, v):
        self.value = v
    

class TestButtonBar(unittest.TestCase):
    
    def test_CreateInstance01(self):
        bar = ButtonBar.ButtonBar()
        ##expect error because there is no function
        with self.assertRaises(KeyError):
            bar.pressButton('Green')

        self.assertTrue("SimpleConstructor")
        
    def test_CreateInstance02(self):
        bar = ButtonBar.ButtonBar(buttonDef={'Green' : 1, 
                                             'Red' : 2}) 

        ##expect error because there is no function
        with self.assertRaises(KeyError):
            bar.pressButton('Red')
            
        self.assertTrue("ButtonPinDefinition Constructor")

    def test_CreateInstance03(self):
        bar = ButtonBar.ButtonBar(buttonDef= {'Green': 26, 'Blue': 19, 'Yellow': 13, 'Red': 6},
                                  actionDict={'Green' : lambda k,p : print("Green Button Press {}{}".format(k,p)),
                                              'Red' : lambda k,p : print("Red Button Press {}{}".format(k,p))})        
        bar.pressButton('Red')
        self.assertTrue("ButtonPinDefinition Constructor")

    def test_CreateInstance04(self):
        greenButtonPressed = Object(False)
        redButtonPressed = Object(False)
                
        bar = ButtonBar.ButtonBar(buttonDef= {'Green': 26, 'Blue': 19, 'Yellow': 13, 'Red': 6},
                                  actionDict={'Green' : lambda k,p : greenButtonPressed.setValue(True),
                                              'Red' : lambda k,p : redButtonPressed.setValue(True)})        
        bar.pressButton('Red')
        self.assertEqual(greenButtonPressed.value, False, "Button callback expected value for non-pressed button")
        self.assertEqual(redButtonPressed.value, True, "Button callback working for local variable through lambda")

    def test_CreateInstance05(self):
        """
        Confirming that a instance method can be called from an object in a lambda function.
        (also avoid lambda lack of assignment issue)
        """
        greenButtonPressed = Object(False)
        redButtonPressed = Object(False)
        
        bar = ButtonBar.ButtonBar(buttonDef= {'Green': 26, 'Blue': 19, 'Yellow': 13, 'Red': 6},
                                  actionDict={'Green' : lambda k,p : greenButtonPressed.setValue(True),
                                              'Red' : lambda k,p : redButtonPressed.setValue(True)})        
        bar.pressButton('Red')
        bar.pressButton('Green')
        self.assertEqual(greenButtonPressed.value, True, "Button callback working for local variable through lambda")
        self.assertEqual(redButtonPressed.value, True, "Button callback working for local variable through lambda")


    def test_ButtonRead01(self):
        bar = ButtonBar.ButtonBar(buttonDef= {'Green': 26, 'Blue': 19, 'Yellow': 13, 'Red': 6},
                                  actionDict={'Green' : lambda k,p : print("Green Button Press {}{}".format(k,p)),
                                              'Red' : lambda k,p : print("Red Button Press {}{}".format(k,p))})        
        dbv = bar.readButtons()
        
        self.assertEqual(dbv['Green'], 26, "Button Read working")
        self.assertEqual(dbv['Blue'], 19, "Button Read working")
        self.assertEqual(dbv['Yellow'], 13, "Button Read working")
        self.assertEqual(dbv['Red'], 6, "Button Read working")



suite = unittest.TestLoader().loadTestsFromTestCase(TestButtonBar)
unittest.TextTestRunner(verbosity=2).run(suite)