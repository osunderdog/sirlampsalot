import time

import ButtonBar
import PICameraCapture

def main():
    ##Button Definitions on sirlapsalot:
    ##{'Green': 26, 'Blue': 19, 'Yellow': 13, 'Red': 6},
    
    imageLocalCacheDir = '/opt/imageCache'

    ##create an instance of the camera capture class (utility class over picamera with configurations)    
    pic = PICameraCapture.PICameraCapture(imageLocalCacheDir)
    pic.appendCameraFunc(PICameraCapture.coCamera)

    ##Button Definitions:
    #Red   : Take a picture
    #Yellow: Toggle between cameras
    #Blue  : Toggle preview on/off
    actionSet = [ButtonBar.ButtonDef(id = 'Red',
                                     pin = 6,
                                     debounce = 200,
                                     function = lambda k : (print(k), print(pic.capture())),
                                     description = 'capture image'),
                 ButtonBar.ButtonDef(id = 'Yellow',
                                     pin = 13,
                                     debounce = 1000,
                                    function = lambda k : (print(k), pic.nextCamera()),
                                     description = 'choose next camera configuration'),
                 ButtonBar.ButtonDef(id = 'Blue',
                                     pin = 19,
                                     debounce = 1000,
                                    function = lambda k : (print(k), pic.togglePreview()),
                                     description = 'Turn preview on/off'),
                 ButtonBar.ButtonDef(id = 'Green',
                                     pin = 26,
                                     debounce = 1000,
                                     function = lambda k : (print(k), print("na")),
                                     description = 'Button not used at this time')
    ]

    ##define the button bar with callbacks for buttons.
    bar = ButtonBar.ButtonBar(buttonSet = actionSet)
    print(bar)
    
    while True:
        time.sleep(0.01)


if __name__ == "__main__":
    main()
