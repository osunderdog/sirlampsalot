
## GPIO  if not available, then should provide some stub
import RPi.GPIO as GPIO

##GPIO pin numbering
GPIO.setmode(GPIO.BCM)

class ButtonDef:
    """defines the information related to a button
    """
    def __init__(self, id, pin, debounce = 0, function = lambda k : print(k), description = ''):
        self.id = id
        self.pin = pin
        self.debounce = debounce
        self.function = function
        self.description = description

    def __str__(self):
        return("buttonID:{};pin:{},desc:{}".format(self.id, self.pin, self.description))

class ButtonBar:
    """GPIO Connected bar of buttons that will control actions on camera.
    """

    def __init__(self, buttonSet):
        """
        Accept a dictionary containing the button and lambda function to perform.
        """
        ##dictionary by id
        self.buttonDict = {i.id: i for i in buttonSet}
        ##revese dict for pin to key lookup
        self.pinButton = {i.pin: i for i in buttonSet}
        
        self._init_gpio()
        self._register_callbacks()

    def __del__(self):
        GPIO.cleanup();

    def __str__(self):
        return("\n".join([str(b) for k,b in self.buttonDict.items()]))

    def _init_gpio(self):
        """Register all the pins that are connected to buttons.
        """
        [GPIO.setup(v.pin,GPIO.IN) for k,v in self.buttonDict.items()]

    def _register_callbacks(self):
        ##for each key, call GPIO add_event_detect.
        [GPIO.add_event_detect(self.buttonDict[k].pin,
                               GPIO.RISING,
                               v.function,
                               self.buttonDict[k].debounce)
         for k,v in self.buttonDict.items()]

    def pressButton(self, k):
        """
        Simulate a button press for pin.  like bb.pressButton('Red')
        """
        ##if nothing in action dictionary for the key this will throw an error. 
        self.buttonDict[k].function(k.pin)

    def readButtons(self):
        """Read current values of each registered pin and return as a tuple
        """
        
        #for each button definition key, 
        return { button: GPIO.input(pin) for pin,button in self.pinButton.items()}

