

import itertools
import os
import time

##picamera if not available, then should provide some stub
import picamera


def bwCamera():
    """gray scale camera because of Saturation...
    """
    camera = picamera.PiCamera()
    camera.resolution = (920,1280)
    camera.saturation = -100
    camera.brightness = 60
    camera.rotation   = 270
    camera.awb_mode = 'auto'

    return(camera)

def coCamera():
    """Color Camera
       The camera will be owned by PICameraCapture until a replacement camera configuration is needed.
    """
    camera = picamera.PiCamera()
    camera.resolution = (920,1280)
    camera.saturation = 0
    camera.brightness = 60
    camera.rotation   = 270
    camera.awb_mode = 'auto'

    return(camera)
            
            
class PICameraCapture:
    """PICamera is a utility class to handle camera configuration and calculate the destination directory for images captured.
    Args:
        dir (str): base path for images created.
        nameTemplate (str): template for the time, random, and extension.
        format (str): image file format -- also the extension for filename ('jpg', 'png')
    """

    PREVIEW_ON = 'on'
    PREVIEW_OFF = 'off'
    
    def __init__(self, dir='/tmp', nameTemplate = 'pi_image_{0}_{1}.{2}', format = 'png', defaultCameraFunc = bwCamera):
        self.uniqueId = itertools.cycle(range(999))
        self.dir = dir
        self.nameTemplate = nameTemplate
        self.format = format
        
        #store a function for default camera creation.
        self.defaultCameraFunc = defaultCameraFunc

        #cameraFunctionList
        self.cameraFuncList = [self.defaultCameraFunc]
        #camera function selection right now.
        self.cameraFuncId = 0

        ##no camera created until we getCamera.
        self.camera = None

        self.previewState = PICameraCapture.PREVIEW_OFF

    def nextCameraFunc(self):
        """Get the next camera function ready for becoming an instance when next needed.
        """
        if(self.cameraFuncId+1 >= len(self.cameraFuncList)):
            self.cameraFuncId = 0
        else:
            self.cameraFuncId += 1

        print("cameraFuncId: {} of {}".format(self.cameraFuncId+1, len(self.cameraFuncList)))
        return(self.cameraFuncList[self.cameraFuncId])

    def appendCameraFunc(self, f):
        """append another camera function to the list of possible camera functions.
        """
        self.cameraFuncList.append(f)
        print("Added a cameraFunc. {}".format(len(self.cameraFuncList)))
    
    def getCamera(self):
        if(self.camera == None):
            self.camera = self.cameraFuncList[self.cameraFuncId]()
        return(self.camera)
    
    def nextCamera(self):
        """ Destroy the current camera if one exists and replace it with a different one.
        """
        #release the camera resources
        if(self.camera != None): 
            self.camera.close()
            self.camera = None
            
        #use function to create a new camera to replace the old one.
        print('creating new camera')
        self.camera = self.nextCameraFunc()()

        #new camera will not have preview on... set it on if the state was on...
        if(self.isPreviewOn()):
            self.start_preview()
            
        return(self.camera)
        
    def uniqueImageFilename(self):
        """Assemble a unique fully qualified filename for the picture that will be taken very soon.        
        """
        return(os.path.join(self.dir,
                            self.nameTemplate.format(time.strftime("%Y%m%d%H%M%S",time.gmtime()),
                                                     next(self.uniqueId),
                                                     self.format
                                                     )
                            ))
##keep a list of lambda functions that cause camera to trigger (push off all the camera configratuion options.

    def capture(self):
        fn = self.uniqueImageFilename()
        self.getCamera().capture(fn)
        return(fn)

    def isPreviewOn(self):
        return(self.previewState == PICameraCapture.PREVIEW_ON)
    
    def togglePreview(self):
        if(self.previewState == PICameraCapture.PREVIEW_OFF):
            ##set to on and start preview
            self.previewState = PICameraCapture.PREVIEW_ON
            self.start_preview()
        else:
            self.previewState = PICameraCapture.PREVIEW_OFF
            self.stop_preview()
    
    def start_preview(self):
        self.getCamera().start_preview()

    def stop_preview(self):
        self.getCamera().stop_preview()

