# sirlampsalot

Introduction
======
*sirlampsalot* is a humble platform for capturing images through a lamp mounted Raspberry Pi with camera and gpio wired buttons.

To run on a raspberry pi with dependent librarys installed:

```
python3 sirlampsalot/runcapture.py
```
As configured here, the program will drop png pictures into the folder

```
/opt/imageCache
```

Hardware
-----
* **Raspberry pi zero w**  
	Using a small raspberry pi because it is mounted on the lamp shade... probably could use older, heavier models but would need to route cables.  Also found that the stock raspi pi camera cable is very short.  

* **Raspberry pi camera**  
	Stock raspberry pi camera.  Need to adjust the focus on the lens to get clear shots at about 18" from work surface.

* **Buttons with pulldown**
    Buttons pulled down with 10k ohm resistor and a 1k ohm current limit resistor.  With current configuration the buttons should be attached to the GPIO pins: 6,13,19 & 26

* **USB power**  
	Routed usb power from wall wart to the raspberry pi.  Eventually will power from lamp switch.

* **mini hdmi**  
	For preview mode -- which is very helpful, need to connect hdmi line to monitor.

* **lamp**  
	Ikea lamp, yellow.
	
* **mounting hardware**  
	Used strip of aluminum and rivets to attach to lamp

Software
----
* python3

Packages
----
* RPi.GPIO
* picamera

REFERENCE
===
More details on this project can be found at [SirLampsalot](http://www.wafermovement.com/?p=198&preview=true)
